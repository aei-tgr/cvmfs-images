# TODO: arrange these notes on the build procedure into a proper script

sudo systemctl start docker
sudo docker login containers.ligo.org

sudo docker build --tag containers.ligo.org/aei-tgr/cvmfs-images/pseob:envs .
sudo docker push containers.ligo.org/aei-tgr/cvmfs-images/pseob:envs
#sudo docker rmi containers.ligo.org/aei-tgr/cvmfs-images/pseob:envs

sudo docker logout containers.ligo.org
sudo systemctl stop docker
