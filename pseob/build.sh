# TODO: arrange these notes on the build procedure into a proper script

#rsync -av --delete ldas-grid.ligo.caltech.edu:/home/jan.steinhoff/public_html/conda/aei-tgr .

sudo systemctl start docker
sudo docker login containers.ligo.org

ENV=pseob-rd
IMGPRE=pseob:rd-
IMG=pseob:rd-latest
sudo docker buildx build --build-arg ENVNAME=$ENV --tag containers.ligo.org/aei-tgr/cvmfs-images/$IMG .
# sudo docker run -it --rm containers.ligo.org/aei-tgr/cvmfs-images/$IMG bash
# sudo docker run -it --rm containers.ligo.org/aei-tgr/cvmfs-images/$IMG conda env export > $ENV-$(date +%Y%m%d).yml
sudo docker tag containers.ligo.org/aei-tgr/cvmfs-images/$IMG containers.ligo.org/aei-tgr/cvmfs-images/$IMGPRE$(date +%Y%m%d)
sudo docker push containers.ligo.org/aei-tgr/cvmfs-images/$IMG
sudo docker push containers.ligo.org/aei-tgr/cvmfs-images/$IMGPRE$(date +%Y%m%d)
#sudo docker rmi containers.ligo.org/aei-tgr/cvmfs-images/$IMG
#sudo docker system prune

#sudo docker logout containers.ligo.org
sudo systemctl stop docker

# pull request for docker_images.txt in https://github.com/opensciencegrid/cvmfs-singularity-sync
