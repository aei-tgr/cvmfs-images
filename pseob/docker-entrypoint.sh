#!/bin/bash

. /opt/conda/etc/profile.d/conda.sh && conda activate `ls /opt/conda/envs` 

exec "$@"
